//
//  main.m
//  123
//
//  Created by FDT14009Mac on 2016/4/16.
//  Copyright © 2016年 GoodArc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
