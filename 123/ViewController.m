//
//  ViewController.m
//  123
//
//  Created by FDT14009Mac on 2016/4/16.
//  Copyright © 2016年 GoodArc. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    
    // 我想找 "莊嘉和"
    // for (條件) {
    // ......
    // }
    
    
    // count => 總數
    
    // for (條件1; 條件2; 條件3) {
    // 條件1 : 初始條件
    // 條件2 : 結束條件 (直到錯為止)
    // 條件3 : 每次執行完要做的事
    // ......
    // }
    // i++ => i = i + 1

    
    // if 如果
    // else 不然的話
    
    NSArray *array = @[   @"ooo",@"ccc",@"aaa",@(100),@(50.3),@"gbn",@"莊嘉和",@(100000)   ];
    
    
    
    for (int i = 0 ; i < array.count; i++) {
        
        if ([array[i] isEqual:@"莊嘉和"]) {
            //找到了
            NSLog(@"找到了");
        }
        else{
            NSLog(@"這是 %@",array[i]);
        }
    }
    // 這是 ooo       i = 0
    // 這是 ccc       1
    // 這是 aaa       2
    // 這是 100       3
    // 這是 50.3      4
    // 這是 gbn       5
    // 找到了          6
    // 這是 100000    7
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
